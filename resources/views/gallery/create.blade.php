


@extends('layouts/main')

@section('content')
  <div class="callout primary">
  <div class="row column">
  <h1>Create Gallery</h1>
  <p class="lead">Create Portfolio Gallery and Start upload Your portfolio Imamge</p>
  </div>
  </div>
  <div class="row small-up-2 medium-up-3 large-up-4">
      <div class="maincontent">
        <div class="card">
              <div class="card-section">
                 {!! Form::open (array('action' =>'GalleryController@store','enctype'=>'multipart/form-data')) !!}

                 {!! Form::label('name','Name') !!}
                 {!! Form::text('name',$value=null,$attributes=['placeholder'=>'Gallery Name','name'=>'name','required'=>'required']) !!}

                 {!! Form::label('description','Description') !!}
                 {!! Form::text('description',$value=null,$attributes=['placeholder'=>'Gallery description','name'=>'description','required'=>'required']) !!}

                 {!! Form::label('cover_image','Cover_image	') !!}
                 {!! Form::file('cover_image') !!}

                    {!! Form::submit('Submit',$attributes=['class'=>'button']) !!}

                 {!! Form::close() !!}
               </div>
          </div>
      </div>
  </div>
@stop
