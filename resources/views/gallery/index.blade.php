

@extends('layouts/main')

@section('content')
  <div class="callout primary">
    <div class="row column">
      <h1>Portfolio Gallery</h1>
      <p class="lead">Create Portfolio Gallery and Start upload Your portfolio Imamge</p>
    </div>
  </div>
  <div class="row small-up-2 medium-up-3 large-up-4">

    @foreach ($gallerys as  $value)
      <div class="column">
        <a href="/gallery/show/{{$value->id}}">
          <img width="250" height="250" class="thumbnail" src="/img/{{ $value->cover_image }}">
        </a>
        <h5>{{ $value->name }}</h5>
        <p>{{ $value->description }}</p>
      </div>
    @endforeach
  </div>
@stop
