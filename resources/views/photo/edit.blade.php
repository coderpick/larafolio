


@extends('layouts/main')

@section('content')
  <div class="callout primary">
  <div class="row column">
  <h1>Upload Portfolio</h1>
  <p class="lead">Upload the portfolio photo to make gallery</p>
  </div>
  </div>
  <div class="row small-up-2 medium-up-3 large-up-4">
      <div class="maincontent">
        <div class="card">
              <div class="card-section">
                 {!! Form::open (array('action' =>'PhotoController@updatedata','enctype'=>'multipart/form-data')) !!}
                  <input type="hidden" name="id" value="{{ $photo->id }}">
                 {!! Form::label('title','Title') !!}
                 {!! Form::text('name',$value=$photo->title,$attributes=['name'=>'title','required'=>'required']) !!}

                 {!! Form::label('description','Description') !!}
                 {!! Form::text('description',$value=$photo->description,$attributes=['name'=>'description','required'=>'required']) !!}

                 {!! Form::label('location','Location') !!}
                 {!! Form::text('location',$value=$photo->location,$attributes=['name'=>'location','required'=>'required']) !!}
                 <div class="upnew">
                   {!! Form::label('image','Portfolio Image	') !!}
                   {!! Form::file('image') !!}
                 </div>
                <div class="preview">
                  <img src="/img/{{$photo->image}}" alt="">
                </div>
                 <input type="hidden" name="gallery_id" value="{{ $photo->gallery_id }}">
                    {!! Form::submit('Update',$attributes=['class'=>'button']) !!}

                 {!! Form::close() !!}
               </div>
          </div>
      </div>
  </div>
@stop
