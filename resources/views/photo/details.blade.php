


@extends('layouts/main')
@section('content')
  <div class="callout primary">
  <div class="row column">
  <h1>{{$photo->title}}</h1>
  <p class="lead">{{$photo->description}}</p>
  <p>{{$photo->location}}</p>
  <a href="/gallery/show/{{$photo->gallery_id}}" class="button primary">Back to Portfolio</a>

  </div>
  </div>
  <div class="row small-up-2 medium-up-3 large-up-4">
      <div class="maincontent">
         <div class="project-img">
           <img src="/img/{{$photo->image}}" alt="">
         </div>
         <div style="text-align:center;margin-top:20px;">
           <?php if (Auth::check()){?>   
           <a  href="/photo/edit/{{$photo->id}}" class="btn-edit">Edit</a>
           <a onclick="return confirm('Are you sure?')" href="/photo/destroy/{{$photo->id}}/{{$photo->gallery_id}}" class="alert btn-delete">Delete</a>
           <?php };?>
         </div>

      </div>
  </div>
@stop
