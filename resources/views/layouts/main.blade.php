
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Laravel||Portfolio</title>
<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
<link rel="stylesheet" href="{{asset('css/style.css') }}">

</head>
<body>



<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-2195009-2', 'auto');
      ga('send', 'pageview');

      ga('create', 'UA-2195009-27', 'auto', {name: "foundation"});
      ga('foundation.send', 'pageview');

    </script>
<div class="off-canvas-wrapper">
<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
<div class="off-canvas position-left reveal-for-large" id="my-info" data-off-canvas data-position="left">
<div class="row column">

<div class="logo">
  <img src="/img/hafizur.png" alt="">
</div>

  <ul class="menu vertical">
    <li class="active"><a href="/">Home</a></li>   
    <?php if (!Auth::check()){?>   
    <li><a href="/login">Login</a></li>
    <li><a href="/register">Register</a></li>
    <?php } ?>
    <?php if (Auth::check()){?>    
      <li><a href="/gallery/create">Create Gallery</a></li>
      <li><a href="/logout">Logout</a></li>     
    <?php } ?>
 
  </ul>

</div>
</div>
<div class="off-canvas-content" data-off-canvas-content>
<div class="title-bar hide-for-large">
<div class="title-bar-left">
<button class="menu-icon" type="button" data-open="my-info"></button>
<span class="title-bar-title">Hafizur Rahman</span>
</div>
</div>
@if (Session::has('message'))
  <div class="alert-box" data-alert>
    {{ Session::get('message') }}
  </div>

@endif

@yield('content')
<hr>
<div class="row">

</div>
</div>
</div>
</div>
<script src="/js/vendor/jquery.js"></script>
<script src="/js/vendor/foundation.min.js"></script>
<script>
      $(document).foundation();
    </script>
<script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
</body>
</html>
