<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Auth;

class GalleryController extends Controller
{
  private $table ='galleris';
    //list gallery
    public function index(){
      //get gallery table data
    $gallerys = DB::table($this->table)->get();
    //Render the view
    	return view('gallery/index',compact('gallerys'));
    }
    //show create From
    public function create(){
        if (!Auth::check()) {
         return redirect()->action('GalleryController@index');
        }
    	  	return view('gallery/create');
    }

    //store gallary item
    public function store(Request $request){
        $name          = $request->input('name');
        $description   = $request->input('description');
        $cover_image   = $request->file('cover_image');
        $owner_id      = 1;
      //check image upload
      if ($cover_image) {
        $cover_image_filename = $cover_image->getClientOriginalName();
        $cover_image->move(public_path('img'),$cover_image_filename);
      } else {
        $cover_image_filename='noimage.jpg';
      }
     // insert gallery image

     DB::table($this->table)->insert(
       [
         'name'        => $name,
         'description' => $description,
         'cover_image' => $cover_image_filename,
         'owner_id'    => $owner_id
       ]
     );
// Set message
\Session::flash('message','Gallery Created');
//Redirect
//return \Redirect::route('gallery.index');
return redirect()->action('GalleryController@index');
    }

    //show gallary photo

    public function show($id){
    	//get galllery
          $gallery = DB::table($this->table)->where('id',$id)->first();
          //get photos
          $photos = DB::table('photos')->where('gallery_id',$id)->get();
          //Render the view
        return view('gallery/show',compact('gallery','photos'));
    }
}
