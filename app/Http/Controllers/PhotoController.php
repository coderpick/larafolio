<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class PhotoController extends Controller
{
    private $table ='photos';
     //portfolio item gallery
    public function create($gallery_id){
       if (!Auth::check()) {
         return redirect()->action('GalleryController@index');
        }
	    return view('photo/create',compact('gallery_id'));
    }


    //store portfolio item
    public function store(Request $request){
            $gallery_id    = $request->input('gallery_id');
            $title         = $request->input('title');
            $description   = $request->input('description');
            $location      = $request->input('location');
            $image         = $request->file('image');
            $owner_id      = 1;
          //check image upload
          if ($image) {
            $image_filename = $image->getClientOriginalName();
            $div = explode('.', $image_filename);
            $file_ext = strtolower(end($div));
            $unique_name = substr(md5(time()), 0, 10).'.'.$file_ext;
            //$unique_name    =  uniqid();
            $image->move(public_path('img'),$unique_name);
          } else {
            $unique_name='noimage.jpg';
          }
         // insert gallery image

         DB::table($this->table)->insert(
           [
             'title'        => $title,
             'description'  => $description,
             'location'     => $location,
             'gallery_id'   =>$gallery_id,
             'image'        => $unique_name,
             'owner_id'     => $owner_id
           ]
         );
      // Set message
      \Session::flash('message','Portfolio item added');
      //Redirect
    // return \Redirect::route('gallery.show',array('id'=>$gallery_id));
     return redirect()->action('GalleryController@show',array('id'=>$gallery_id));
    }

    //show portfoli details
    public function details($id){
    	     $photo = DB::table($this->table)->where('id',$id)->first();
          //get photos
          return view('photo/details',compact('photo'));
    }

//update
public function edit($id){
    if (!Auth::check()) {
         return redirect()->action('GalleryController@index');
        }
  $photo = DB::table($this->table)->where('id',$id)->first();
 //get photos
 return view('photo/edit',compact('photo'));
}

//
public function updatedata(Request $request){
            $id            = $request->input('id');
            $gallery_id    = $request->input('gallery_id');
            $title         = $request->input('title');
            $description   = $request->input('description');
            $location      = $request->input('location');
            $image         = $request->file('image');
            $owner_id      = 1;
          //check image upload
          if ($image) {
            $image_filename = $image->getClientOriginalName();
            $div = explode('.', $image_filename);
            $file_ext = strtolower(end($div));
            $unique_name = substr(md5(time()), 0, 10).'.'.$file_ext;
            //$unique_name    =  uniqid();
            $image->move(public_path('img'),$unique_name);
               
            // update gallery image
             DB::table($this->table)->where('id',$id)->update(
               [
                 'title'        => $title,
                 'description'  => $description,
                 'location'     => $location,
                 'gallery_id'   =>$gallery_id,
                 'image'        => $unique_name,
                 'owner_id'     => $owner_id
               ]
             );
          } else {
            DB::table($this->table)->where('id',$id)->update(
                    [
                      'title'        => $title,
                      'description'  => $description,
                      'location'     => $location,
                      'gallery_id'   =>$gallery_id,                     
                      'owner_id'     => $owner_id
                    ]
              );
          }
      
      // Set message
      \Session::flash('message','Portfolio item update successfully');
      //Redirect
    // return \Redirect::route('gallery.show',array('id'=>$gallery_id));
     return redirect()->action('GalleryController@show',array('id'=>$gallery_id));

}
//delete
public function destroy($id,$gallery_id){
    $photo = DB::table($this->table)->where('id',$id)->delete();

    //  unlink(public_path('img/'));
    // Set message
    \Session::flash('message','Portfolio item Deleted Successfully');
    //Redirect
//   return \Redirect::route('gallery.show',array('id'=>$gallery_id));
  return redirect()->action('GalleryController@show',array('id'=>$gallery_id));
    }
}
